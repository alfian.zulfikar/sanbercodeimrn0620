// 1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
class Score {
    constructor(points) {
        this._subject;
        this._points = points;
        this._email;
    }

    average() {
        var result = 0;
        if (this._points.length > 0) {
            // console.log("ok");
            for(var i = 0; i < this._points.length; i++) {
                result = result + this._points[i];
            }
            result = result / this._points.length;
            // console.log("Rata-rata: " + result);
            return result;
        } else {
            // console.log(this._points);
            return this._points;
        }
    }
}

var data1 = [1,2,3,4];
var obj = new Score(data1);
console.log(obj.average());

// 2. SOAL Create Score (10 Poin + 5 Poin ES6)
const data = [
    ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
]

function viewScores(data, subject) {
    var arr = [];
    var index = 0;
    for(var i = 0; i < data.length; i++) {
        var email = data[i][0];
        var pointQueue = data[i].slice(1,3)
        var s = new Score(pointQueue);
        var points = s.average()
        if(email != "email") {
            var obj = {
                email: email,
                subject: subject,
                points: points
            }
            arr[index] = obj;
            index++;
        }
        
    }
    return arr;
}

// console.log(viewScores(data, "quiz-1"));
// TEST CASE
console.log(viewScores(data, "quiz-1"))
console.log(viewScores(data, "quiz-2"))
console.log(viewScores(data, "quiz-3"))

//  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
function recapScores(data) {
    // viewScores(data, "quiz-1")
    var result = "";
    for(var i = 0; i < (data.length-1); i++) {
        result += (i+1) + ". Email: " + viewScores(data, "quiz-1")[i].email + "\n";
        result += "Rata-rata: " + viewScores(data, "quiz-1")[i].points + "\n";
        if(viewScores(data, "quiz-1")[i].points > 90) {
            result += "Predikat: honour" + "\n";
        } else if(viewScores(data, "quiz-1")[i].points > 80) {
            result += "Predikat: graduate" + "\n";
        } else {
            result += "Predikat: participant" + "\n";
        }
        result += "\n";
        
    // console.log(viewScores(data, "quiz-1")[i].email);

    }
    console.log(result)

}
  
recapScores(data);

// NO 3 sudah diperbaiki *thumbsUp