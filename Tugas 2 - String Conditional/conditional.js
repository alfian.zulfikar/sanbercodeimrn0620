// if-else
var nama = "Jane";
var peran = "Penyihir";

if (nama == "") {
    console.log("Nama harus diisi!");
} else if (peran == "" ) {
    console.log("Halo " + nama + ", Pilih peranmu untuk bermain Game!");
} else if (peran == "Penyihir"){
    console.log("Selamat datang di Dunia Werewolf, " + nama);
    console.log("Halo " + peran + " "+ nama + ", kamu dapat melihat siapa yang menjadi werewolf!")
} else if (peran == "Guard"){
    console.log("Selamat datang di Dunia Werewolf, " + nama);
    console.log("Halo " + peran + " "+ nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
} else if (peran == "Werewolf"){
    console.log("Selamat datang di Dunia Werewolf, " + nama);
    console.log("Halo " + peran + " "+ nama + ", Kamu akan memakan mangsa setiap malam!")
}

console.log("")

// switch-case
var tanggal = 1; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 1; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 2020; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

switch(bulan) {
    case 1:   { bulan = 'Januari'; break; }
    case 2:   { bulan = 'Februari'; break; }
    case 3:   { bulan = 'Maret'; break; }
    case 4:   { bulan = 'April'; break; }
    case 5:   { bulan = 'Mei'; break; }
    case 6:   { bulan = 'Juni'; break; }
    case 7:   { bulan = 'Juli'; break; }
    case 8:   { bulan = 'Agustus'; break; }
    case 9:   { bulan = 'September'; break; }
    case 10:   { bulan = 'Oktober'; break; }
    case 11:   { bulan = 'November'; break; }
    case 12:   { bulan = 'Desember'; break; }
    default:  { console.log('-'); }}

console.log(tanggal + " " + bulan + " " + tahun);