// No.1
function teriak() {
    return "Halo Sanbers!"
}

console.log(teriak())
console.log("")

// No.2
function multiply(param1, param2) {
    return param1*param2
}

var num1 = 12
var num2 = 4

var hasilKali = multiply(num1, num2)
console.log(hasilKali)
console.log("")

// No.3
function introduce(nama, umur, alamat, hobi) {
    return "Nama saya " + nama + ", umur saya " + umur + " tahun, alamat saya di " + alamat + ", dan saya punya hobby yaitu " + hobi + "!"
}

var name = "Agus"
var age = "30"
var alamat = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"

var perkenalan = introduce(name, age, alamat, hobby)
console.log(perkenalan)