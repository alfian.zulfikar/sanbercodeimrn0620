// A. Balik String (10 Poin)
function balikString(data) {
    var result = "";
    var charTotal = data.length;
    for (var i = charTotal; i > 0; i--) {
        result += data[i-1];
    }
    return result;
}

// B. Palindrome (10 poin)
function palindrome(data) {
    var reverse = "";
    var charTotal = data.length;
    for (var i = charTotal; i > 0; i--) {
        reverse += data[i-1];
    }
    if (reverse == data) {
        return true;
    } else {
        return false;
    }
}

// C. Bandingkan Angka (10 poin)
function bandingkan(num1 = 0, num2 = 0) {
    if (num1 < 0 || num2 < 0) {
        return -1;
    } else if (num1 == num2) {
        return -1;
    } else {
        if (num1 > num2) {
            return num1;
        } else {
            return num2;
        }
    }
}

// TEST CASES BalikString

console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah

console.log("");

// TEST CASES Palindrome
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false

console.log("");

// TEST CASES Bandingkan Angka
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18