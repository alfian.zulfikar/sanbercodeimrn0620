// A. Ascending Ten (10 poin)
function AscendingTen(num) {
    var result = "";
    if (num != null) {
        for (var i = 0; i < 10; i++) {
            result += String(num) + " ";
            num++;
        }
        return result;
    } else {
        return -1;
    }
}

// B. Descending Ten (10 poin)
function DescendingTen(num) {
    var result = "";
    if (num != null) {
        for (var i = 0; i < 10; i++) {
            result += String(num) + " ";
            num--;
        }
        return result;
    } else {
        return -1;
    }
}

// C. Conditional Ascending Descending (15)
function ConditionalAscDesc(reference, check) {
    var result = "";
    if (reference != null &&  check != null) {
        for (var i = 0; i < 10; i++) {
            result += String(reference) + " ";
            if (check%2 == 0) {
                reference--;
            } else {
                reference++;
            }
        }
        return result;
    } else {
        return -1;
    }
}

// D. Papan Ular Tangga (35)
function ularTangga() {
    var num = 100;
    var result = "";
    var line = "odd";
    var temp = "";
    for (var i = 0; i < 10; i++) {
        for (var j = 0; j < 10; j++) {
            if (line == "odd") {
                result += String(num) + " ";
            } else {
                temp = String(num) + " " + temp;
            }
            num--;
        }
        if (line == "odd") {
            line = "even";
            // result += "\n";
        } else {
            line = "odd";
            result += "\n" + temp;
            result += "\n";
            // result = result + "\n";
        }
        temp = "";
    }
    return result;
}


// TEST CASES Ascending Ten
console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1

console.log("");

// TEST CASES Descending Ten
console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1

console.log("");

// TEST CASES Conditional Ascending Descending
console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)) // -1
console.log(ConditionalAscDesc()) // -1

console.log("");

// TEST CASE Ular Tangga
console.log(ularTangga()) 
