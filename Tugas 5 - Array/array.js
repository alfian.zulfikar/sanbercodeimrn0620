// No.1 (Range)
console.log("NO.1 (RANGE)");
function range(startNum, finishNum) {
    var number1 = [];
    if (startNum != null && finishNum != null) {
        if (startNum < finishNum) {
            for (var i = startNum; i <= finishNum; i++ ) {
                number1.push(i);
            }
            return number1;
        } else {
            for (var i = startNum; i >= finishNum; i-- ) {
                number1.push(i);
            }
            return number1;
        }
    } else {
        return -1
    }
}

console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11,18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1 
console.log("");

// No.2 (Range with Step)
console.log("NO.2 (RANGE WITH STEP)");
function rangeWithStep(startNum, finishNum, step) {
    var number2 = [];
    if (startNum != null && finishNum != null && step != null) {
        if (startNum < finishNum) {
            for (var i = startNum; i <= finishNum; i += step ) {
                // console.log(i);
                number2.push(i);
            }
            return number2;
        } else {
            for (var i = startNum; i >= finishNum; i -= step ) {
                number2.push(i);
            }
            return number2;
        }
    } else {
        return -1
    }
}

console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5] 
console.log("");

// No.3 (Sum of Range)
console.log("NO.3 (SUM OF RANGE)");
function sum(firstNum, lastNum, step = 1) {
    var number3, result;
    var result = 0;
    if (firstNum != null && lastNum != null) {
        number3 = rangeWithStep(firstNum, lastNum, step);
        for (var i = 0; i < number3.length; i++) {
            result += number3[i];
        }
        return result;
    } else if (firstNum == null && lastNum == null) {
        return 0;
    } else {
        return firstNum;
    }
}

console.log(sum(1,10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15,10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0
console.log("");

// No.4 (Array Multidimensi)
console.log("NO.4 (ARRAY MULTDIMENSI)");
function dataHandling(data) {
    var result = "";
    for (var i = 0; i < data.length; i++) {
        result = result.concat("Nomor ID: " + data[i][0] + "\n" + "Nama Lengkap: " + data[i][1] + "\n" + "TTL: " + data[i][2] + " " + data[i][3] + "\n" + "Hobi: " + data[i][4] + "\n\n");
    }
    return result;
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];

console.log(dataHandling(input));

// No.5 (Balik Kata)
console.log("NO.5 (BALIK KATA)");
function balikKata(kata) {
    var temp = [];
    var charTotal = kata.length;
    for (var i = charTotal; i > 0; i--) {
        temp.push(kata[i-1]);
    }
    var result = temp.join("");
    return result;
}

console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I
console.log("");

// No.6 (Metode Array)
console.log("NO.6 (METODE ARRAY)");
function dataHandling2(data) {
    // memperbaiki array dengan splice
    data.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
    data.splice(4, 1, "Pria", "SMA Internasional Metro");
    console.log(data);

    // menampilkan nama bulan dari elemen yang berisikan tanggal/bulan/tahun
    var date = data[3].split("/");
    switch(date[1]) {
        case "01": { console.log("Januari"); break; }
        case "02": { console.log("Februari"); break; }
        case "03": { console.log("Maret"); break; }
        case "04": { console.log("April"); break; }
        case "05": { console.log("Mei"); break; }
        case "06": { console.log("Juni"); break; }
        case "07": { console.log("Juli"); break; }
        case "08": { console.log("Agustus"); break; }
        case "09": { console.log("September"); break; }
        case "10": { console.log("Oktober"); break; }
        case "11": { console.log("November"); break; }
        case "12": { console.log("Desember"); break; }
        default: { console.log("-") }
    }

    // menyortir array hasil split dari tanggal/bulan/tahun secara descanding
    date.sort(function(a,b) {return b-a});
    console.log(date);

    // menggabungkan semua elemen pada array hasil split dari elemen tanggal/bulan/tahun dengan join
    date = data[3].split("/");
    resultJoin = date.join("-");
    console.log(resultJoin);

    // pembatasan nama (elemen ke 2) sebanyak 15 karakter dengan slice
    if (data[1].length > 15) {
        var temp = [];
        for (var i = 0; i < data[1].length; i++) {
            temp.push(data[1][i]);
        }
        var result = temp.slice(0, 14);
        result = result.join("");
        console.log(result);
    }
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);