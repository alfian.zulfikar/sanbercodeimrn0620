import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Latihan from './Latihan/Latihan1/index'
import RegistrasiLatihan from './Latihan/Latihan2/Registrasi';
import AboutLatihan from './Latihan/Latihan2/About';
import LoginLatihan from './Latihan/Latihan2/Login';
import Youtube from './Tugas/Tugas12/App';
import LoginDevhouse from './Tugas/Tugas13/Login';
import RegistrasiDevhouse from './Tugas/Tugas13/Registrasi';
import AboutDevhouse from './Tugas/Tugas13/About';
import SkillScreen from './Tugas/Tugas14/skillScreen';
import ToDoApp from './Tugas/Tugas14/App';
import Tugas15 from './Tugas/Tugas15/Index';
import TugasNavigation from './Tugas/TugasNavigation/index';
import Quiz3 from './Quiz3/index';

export default function App() {
  return (
    // --------Latihan--------
    // <Latihan />
    // <LoginLatihan />
    // <RegistrasiLatihan />
    // <AboutLatihan/>
    
    // --------Tugas 12--------
    // <Youtube />

    // --------Tugas 13--------
    // <LoginDevhouse />
    // <RegistrasiDevhouse />
    // <AboutDevhouse />

    //--------Tugas 14--------
    // <ToDoApp/>
    // <SkillScreen />

    //--------Tugas 15--------
    // <Tugas15 />

    //--------Tugas Navigation--------
    <TugasNavigation />

    //--------Quiz 3--------
    // <Quiz3 />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
