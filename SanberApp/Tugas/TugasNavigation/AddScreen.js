import React from 'react';
import { StyleSheet, Text, View} from 'react-native';

export default class AddScreen extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.text}>Halaman Tambah</Text>
            </View>
        );
    };
}

const styles = StyleSheet.create({ 
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
})