import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';

import LoginScreen from './LoginScreen';
import AboutScreen from './AboutScreen';
import SkillScreen from './skillScreen';
import ProjectScreen from './ProjectScreen';
import AddScreen from './AddScreen';

const Drawer = createDrawerNavigator();
const Tabs = createBottomTabNavigator();

const RootStack = createStackNavigator();

const TabsScreen = () => {
    return (
      <Tabs.Navigator>
        <Tabs.Screen name="Skill" component={SkillScreen} />
        <Tabs.Screen name="Project" component={ProjectScreen} />
        <Tabs.Screen name="Add" component={AddScreen} />
      </Tabs.Navigator>
    );
};

const DrawerScreen = () => {
    return (
        <Drawer.Navigator>
            <Drawer.Screen name="Skill" component={TabsScreen} />
            <Drawer.Screen name="About" component={AboutScreen} />
        </Drawer.Navigator>
    );
};

export default () => {
    return (
        <NavigationContainer>
            <RootStack.Navigator>
                <RootStack.Screen name="Login" component={LoginScreen} />
                <RootStack.Screen name="Drawer" component={DrawerScreen} />
            </RootStack.Navigator>
        </NavigationContainer>
    );
}