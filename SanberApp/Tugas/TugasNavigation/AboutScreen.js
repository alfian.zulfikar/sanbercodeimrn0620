import React from 'react';
import { ScrollView, StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { FontAwesome } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';

const {width : WIDTH} = Dimensions.get('window');

export default class AboutScreen extends React.Component {
    render() {
        return (
            <View style={styles.container} >
            <ScrollView >
            <View style={styles.containerIn} >
                <View style={styles.headerContainer}>
                    <Text style={styles.headerTitle}>Tentang Saya</Text>
                </View>
                <View style={styles.UserContainer}>
                    <Icon style={styles.accountImage} name="account-circle" size={200} />
                    <Text style={styles.accountName}>Mukhlis Hanafi</Text>
                    <Text style={styles.accountDesc}>React Native Developer</Text>
                </View>
                <View style={styles.infoContainer}>
                    <Text style={styles.infoTitle}>Portofolio</Text>
                    <View style={styles.line}></View>
                    <View style={styles.infoArea}>
                        <View style={styles.portofolioList}>
                            <View style={styles.portofolioItem}>
                                <FontAwesome name="gitlab" style={styles.portofolioLogo} size={42.19}/>
                                <Text style={styles.portofolioAccount}>@mukhlish</Text>
                            </View>
                            <View style={styles.portofolioItem}>
                                <AntDesign name="github" style={styles.portofolioLogo} size={42.19} />
                                <Text style={styles.portofolioAccount}>@mukhlis-h</Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.infoContainer}>
                    <Text style={styles.infoTitle}>Hubungi Saya</Text>
                    <View style={styles.line}></View>
                    <View style={styles.infoArea}>
                        <View style={styles.hubungiSayaList}>
                            <View style={styles.hubungiSayaItem}>
                                <FontAwesome name="facebook-square" style={styles.hubungiSayaLogo} size={42.19}/>
                                <Text style={styles.hubungiSayaAccount}>mukhlis.hanafi</Text>
                            </View>
                            <View style={styles.hubungiSayaItem}>
                                <FontAwesome name="instagram" style={styles.hubungiSayaLogo} size={40}/>
                                <Text style={styles.hubungiSayaAccount}>@mukhlis_hanafi</Text>
                            </View>
                            <View style={styles.hubungiSayaItem}>
                                <FontAwesome name="twitter" style={styles.hubungiSayaLogo} size={40}/>
                                <Text style={styles.hubungiSayaAccount}>@mukhlish</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
            </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    containerIn: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 50,
        backgroundColor: '#fff'

    },
    headerContainer: {
        marginTop: 64,
        alignItems: 'center',
    },
    headerTitle: {
        fontSize: 36,
        fontFamily: 'Roboto',
        fontWeight: 'bold',
        color: '#003366'
    },
    UserContainer: {
        marginTop: 24,
        alignItems: 'center',
    },
    accountImage: {
        color: '#efefef',
        marginTop: 12
    },
    accountName: {
        fontSize: 24,
        fontFamily: 'Roboto',
        fontWeight: 'bold',
        color: '#003366'
    },
    accountDesc: {
        fontSize: 16,
        fontFamily: 'Roboto',
        fontWeight: 'bold',
        color: '#3EC6FF'
    },
    infoContainer: {
        width: WIDTH -10,
        flex: 1,
        borderRadius: 16,
        backgroundColor: '#efefef',
        marginTop: 16
    },
    infoTitle: {
        fontSize: 18,
        fontFamily: 'Roboto',
        color: '#003366',
        paddingLeft: 10
    },
    line: {
        borderBottomColor: '#003366',
        borderBottomWidth: 1
    },
    infoArea: {
        flex: 1,
        justifyContent: 'center'
    },
    portofolioList: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        paddingTop: 15,
        paddingBottom: 15
    },
    portofolioItem: {
        alignItems: 'center'
    },
    portofolioLogo: {
        color: '#3EC6FF'
    },
    portofolioAccount: {
        color: '#003366',
        fontWeight: 'bold',
        fontSize: 16
    },
    hubungiSayaList: {
        flexDirection: 'column',
        justifyContent: 'space-around',
        // alignItems: 'center',
        paddingTop: 15,
        paddingBottom: 15
    },
    hubungiSayaItem: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 20
    },
    hubungiSayaLogo: {
        color: '#3EC6FF',
        paddingLeft: 100
    },
    hubungiSayaAccount: {
        color: '#003366',
        fontWeight: 'bold',
        fontSize: 16,
        paddingLeft: 10
    },
})