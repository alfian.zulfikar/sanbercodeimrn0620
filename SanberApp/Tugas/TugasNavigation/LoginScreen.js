import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Dimensions, ScrollView, Button } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const {width : WIDTH} = Dimensions.get('window')

const ScreenContainer = ({ children }) => (
    <View style={styles.container}>{children}</View>
);

export const DrawerButton = ({ navigation }) => (
    <ScreenContainer>
      <Button title="Drawer" onPress={() => navigation.toggleDrawer()} />
    </ScreenContainer>
);

export default function LoginScreen({ navigation }) {
    // render({ navigation }) {
        return (
            <View style={styles.container} >
            <ScrollView >
            <View style={styles.containerIn} >
                <View style={styles.logoContainer}>
                    <Image source={require("./images/logo.png")} style={styles.logo} />
                    <Text style={styles.loginTitle}>Login</Text>
                </View>
                
                <View style={{marginTop: 40}}>
                    <Text style={styles.inputTitle}>Username / Email</Text>
                    <TextInput style={styles.input} />
                </View>
                <View>
                    <Text style={styles.inputTitle}>Password</Text>
                    <TextInput style={styles.input} secureTextEntry={true} />
                </View>

                <TouchableOpacity style={styles.btnLogin}>
                    <Text style={styles.textBtn}>MASUK</Text>
                </TouchableOpacity>

                <View>
                    <Text style={styles.atau}>ATAU</Text>
                </View>

                <TouchableOpacity style={styles.btnDaftar}>
                    <Text style={styles.textBtn}>DAFTAR ?</Text>
                </TouchableOpacity>

                <View>
                    {DrawerButton}
                </View>

                {/* <TouchableOpacity style={styles.btnDaftar}>
                    <Text style={styles.textBtn} onPress={() => navigation.toggleDrawer()}>DRAWER</Text>
                </TouchableOpacity> */}
                {/* <Button title="Drawer" onPress={() => navigation.toggleDrawer()} /> */}
            </View>
            </ScrollView>
            </View>
        );
    // }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    containerIn: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 50,
        backgroundColor: '#fff'
    },
    logoContainer: {
        marginTop: 63,
        alignItems: 'center'
    },
    logo: {
        width:375,
        height:102
    },
    loginTitle: {
        fontSize: 24,
        fontFamily: 'Roboto',
        marginTop: 70,
        color: '#003366'
        // lineHeight: 28
    },
    input: {
        width: 294,
        height: 40,
        fontSize: 16,
        borderColor: '#003366',
        color: '#003366',
        borderWidth: 1,
        marginBottom: 16,
        paddingLeft: 20
    },
    inputTitle: {
        color: '#003366'
    },
    btnLogin: {
        width: 140,
        height: 40,
        backgroundColor: '#3ec6ff',
        borderRadius: 16,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 32,
        marginBottom: 10
    },
    textBtn: {
        fontSize: 20,
        color: 'white'
    },
    atau: {
        fontSize: 20,
        color: '#3ec6ff',
        marginBottom: 10
    },
    btnDaftar: {
        width: 140,
        height: 40,
        backgroundColor: '#003366',
        borderRadius: 16,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10
    }
})