import React from 'react';
import { ScrollView, StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Dimensions, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import { isRequired } from 'react-native/Libraries/DeprecatedPropTypes/DeprecatedColorPropType';
import { FontAwesome5 } from '@expo/vector-icons';
import data from './skillData.json';
import SkillItem from './components/SkillItem';

const {width : WIDTH} = Dimensions.get('window');

export default class Login extends React.Component {
    render() {
        return (
            <View style={styles.container} >
                    {/* <View style={styles.containerIn} > */}
                        <View style={styles.headerContainer}>
                            <Image source={require("./images/logo.png")} style={styles.logo} />
                        </View>
                        <View style={styles.profileContainer}>
                            <Icon style={styles.accountImage} name="account-circle" size={40} />
                            <View style={styles.profileGreeting}>
                                <Text style={styles.textHai}>Hai,</Text>
                                <Text style={styles.profileName}>Mukhlis Hanafi</Text>
                            </View>
                        </View>
                        <View style={styles.skillContainer}>
                            <Text style={styles.skillTitle}>SKILL</Text>
                            <View style={styles.line}></View>
                            <View style={styles.categoryContainer}>
                                <View style={styles.categoryList}>
                                    <Text style={styles.categoryItem}>Library/Framework</Text>
                                </View>
                                <View style={styles.categoryList}>
                                    <Text style={styles.categoryItem}>Bahasa Pemrograman</Text>
                                </View>
                                <View style={styles.categoryList}>
                                    <Text style={styles.categoryItem}>Teknologi</Text>
                                </View>
                            </View>
                            <View style={styles.skillList}>
                                {/* <View style={styles.skillItem}>
                                    <MaterialCommunityIcons name={skill.items[0].iconName} size={100} color="black" />
                                    <View style={styles.skillDesc}>
                                        <Text style={styles.skillName}>React Native</Text>
                                        <Text style={styles.skillCategory}>Library / Framework</Text>
                                        <View style={styles.precentageArea}>
                                            <Text style={styles.precentage}>50%</Text>
                                        </View>
                                    </View> */}
                                    {/* <Icon name="navigate-next" size={100} color="black" /> */}
                                    <FlatList
                                    data={data.items}
                                    renderItem={(skill)=><SkillItem skill={skill.item} />}
                                    // keyExtractor={(item)=>item.id}
                                    // ItemSeparatorComponent={()=><View style={{height:0.5,backgroundColor:'#e5e5e5'}} />}
                                    />
                                {/* </View> */}
                                {/* <SkillItem skill={data.items[2]} /> */}
                            </View>
                        </View>
                    {/* </View> */}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center'
    },
    // containerIn: {
    //     flex: 1,
    //     alignItems: 'center',
    //     // justifyContent: 'center',
    //     // paddingBottom: 50,
    //     width: 340,
    //     paddingLeft: 20,
    //     paddingRight: 20,
    //     // backgroundColor: 'grey'
    // },
    headerContainer: {
        marginTop: 10,
        width: 340,
        alignItems: 'flex-end',
    },
    logo: {
        width: 187,
        height: 51
    },
    profileContainer: {
        flexDirection: 'row',
        width: 340
    },
    accountImage: {
        color: '#3EC6FF'
    },
    profileGreeting: {
        paddingLeft: 11
    },
    textHai: {
        fontSize: 12,
        color: '#003366'
    },
    profileName: {
        fontSize: 16,
        color: '#003366'
    },
    skillContainer: {
        width: 340,
        marginTop: 16,
        // flex: 1
    },
    line: {
        borderBottomColor: '#3EC6FF',
        borderBottomWidth: 4,
        width: 340
    },
    skillTitle: {
        fontSize: 36,
        color: '#003366',
        flex:1
    },
    categoryContainer: {
        width: 340,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 10
    },
    categoryList: {
        backgroundColor: '#3EC6FF',
        borderRadius: 8
    },
    categoryItem: {
        fontSize: 12,
        fontWeight: 'bold',
        padding: 9,
        color: '#003366'
    },
    skillList: {
        marginTop: 10,
        width: 340,
        height: 450
        // flex: 1
    },
    skillItem: {
        backgroundColor: '#B4E9FF',
        borderRadius: 8,
        shadowColor: "#000",
        // shadowOffset: {
        //     width: 0,
        //     height: 2,
        // },
        shadowOpacity: 0.5,
        shadowRadius: 3.84,
        elevation: 3,
        padding: 10,
        flexDirection: 'row'
    },
    skillName: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#003366'
    },
    skillCategory: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#3ec6ff'
    },
    precentageArea: {
        alignItems: 'flex-end'
    },
    precentage: {
        fontSize: 48,
        fontWeight: 'bold',
        color: '#fff'
    }
})