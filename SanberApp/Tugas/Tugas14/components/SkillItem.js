import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
// import skill from '../skillData.json';

export default class SkillItem extends React.Component {
    render() {
        let skill = this.props.skill;
        // alert(skill.skillName)
        return (
            <View style={styles.skillItem}>
                <MaterialCommunityIcons name={skill.iconName} size={100} color="black" />
                <View style={styles.skillDesc}>
                    <Text style={styles.skillName}>{skill.skillName}</Text>
                    <Text style={styles.skillCategory}>{skill.category}</Text>
                    <View style={styles.precentageArea}>
                        <Text style={styles.precentage}>{skill.percentageProgress}</Text>
                    </View>
                </View>
                <Icon name="navigate-next" size={100} color="black" />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    skillItem: {
        backgroundColor: '#B4E9FF',
        borderRadius: 8,
        shadowColor: "#000",
        // shadowOffset: {
        //     width: 0,
        //     height: 2,
        // },
        shadowOpacity: 0.5,
        shadowRadius: 3.84,
        elevation: 3,
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    skillName: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#003366'
    },
    skillCategory: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#3ec6ff'
    },
    precentageArea: {
        alignItems: 'flex-end'
    },
    precentage: {
        fontSize: 48,
        fontWeight: 'bold',
        color: '#fff'
    }
});