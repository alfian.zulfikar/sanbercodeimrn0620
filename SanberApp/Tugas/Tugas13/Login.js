import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Dimensions, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const {width : WIDTH} = Dimensions.get('window')

export default class Login extends React.Component {
    render() {
        return (
            <View style={styles.container} >
            <ScrollView >
            <View style={styles.containerIn} >
                <View style={styles.logoContainer}>
                    <Image source={require("./images/logo.png")} style={styles.logo} />
                </View>
                
                <View style={{marginTop: 68}}>
                    <Text style={styles.inputTitle}>Username / Email</Text>
                    <TextInput style={styles.input} />
                </View>
                <View>
                    <Text style={styles.inputTitle}>Password</Text>
                    <TextInput style={styles.input} secureTextEntry={true} />
                </View>

                <TouchableOpacity style={styles.btnLogin}>
                    <Text style={styles.textBtn}>LOG IN</Text>
                </TouchableOpacity>

                <View style={styles.questionContainer}>
                    <Text style={styles.questionText}>Don't have an account?</Text>
                    <TouchableOpacity style={styles.btnDaftar}>
                        <Text style={styles.textBtn}>Sign Up</Text>
                    </TouchableOpacity>
                </View>

            </View>
            </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#04154F'
    },
    containerIn: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 50
    },
    logoContainer: {
        marginTop: 80,
        alignItems: 'center'
    },
    logo: {
        width:224,
        height:142
    },
    input: {
        width: 266,
        height: 40,
        fontSize: 16,
        color: '#04154F',
        backgroundColor: '#ffffff',
        borderWidth: 1,
        marginBottom: 20,
        paddingLeft: 20,
        borderRadius: 50
    },
    inputTitle: {
        paddingLeft: 20,
        color: '#ffffff',
        fontSize: 13
    },
    btnLogin: {
        width: 266,
        height: 40,
        backgroundColor: '#5684F9',
        borderRadius: 16,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 15,
        marginBottom: 10
    },
    textBtn: {
        fontSize: 13,
        fontWeight: 'bold',
        color: '#ffffff'
    },
    questionContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 71
    },
    questionText: {
        fontSize: 13,
        color: '#ffffff'
    },
    textBtn: {
        fontSize: 13,
        fontWeight: 'bold',
        marginLeft: 10,
        color: '#ffffff'
    }
})