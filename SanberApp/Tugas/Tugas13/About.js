import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Dimensions, ScrollView } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

const {width : WIDTH} = Dimensions.get('window')

export default class Login extends React.Component {
    render() {
        return (
            <View style={styles.container} >
            <ScrollView >
            <View style={styles.containerIn} >
                <View style={styles.headerContainer}>
                    <View style={styles.option}>
                        <TouchableOpacity>
                            <Ionicons name="md-arrow-round-back" size={24} style={styles.back} />
                        </TouchableOpacity>
                        <View style={styles.pageTitle}>
                            <Text style={styles.headerTitle}>ABOUT ME</Text>
                        </View>
                    </View>
                    <Image source={require("./images/profile.png")} style={styles.profileImage} />
                </View>
                <View style={styles.contentContainer}>
                    <Text style={styles.contentName}>ALFIAN ZULFIKAR</Text>
                    <View style={styles.line}></View>
                    <Text style={styles.contentDesc}>Graphic Designer | App Developer</Text>
                    <Text style={styles.findMeText}>Find Me</Text>
                    <View style={styles.findMeContainer}>
                        <TouchableOpacity>
                            <Image source={require("./images/facebook.png")} style={styles.findMeImage} />
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Image source={require("./images/twitter.png")} style={styles.findMeImage} />
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Image source={require("./images/instagram.png")} style={styles.findMeImage} />
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.findMeText}>My Portofolio</Text>
                    <View style={styles.findMeContainer}>
                        <TouchableOpacity>
                            <Image source={require("./images/github.png")} style={styles.findMeImage} />
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
            </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff'
    },
    containerIn: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 50
    },
    headerContainer: {
        height: 316,
        width: WIDTH,
        backgroundColor: '#04154F',
        borderBottomLeftRadius: 50,
        borderBottomRightRadius: 50,
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.5,
        shadowRadius: 3.84,
        elevation: 5,
    },
    option: {
        width: WIDTH,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginTop: 40
    },
    back: {
        color: "#ffffff",
        paddingLeft: 20
    },
    pageTitle: {
        width: WIDTH,
        position: 'absolute',
        alignItems: 'center'
    },
    headerTitle: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#ffffff',
        marginTop: 5
    },
    profileImage: {
        height: 160,
        width: 160,
        marginTop: 46
    },
    contentContainer: {
        flex: 1,
        alignItems: 'center'
    },
    contentName: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#04154F',
        letterSpacing: 2,
        marginTop: 33
    },
    line: {
        borderBottomColor: '#5684F9',
        borderBottomWidth: 1,
        width: 235
    },
    contentDesc: {
        fontSize: 12,
        color: '#04154F'
    },
    findMeText: {
        fontSize: 14,
        fontWeight: 'bold',
        color: '#04154F',
        marginTop: 26
    },
    findMeContainer: {
        width: 235,
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 12
    },
    findMeImage: {
        height: 48,
        width: 48
    },
})