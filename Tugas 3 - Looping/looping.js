console.log("No. 1 Looping While")
console.log("LOOPING PERTAMA");
var flag1 = 2;
while(flag1 <= 20) {
    console.log(flag1 + " - I love coding");
    flag1 += 2;
}
console.log("LOOPING KEDUA");
var flag2 = 20;
while(flag2 >= 2) {
    console.log(flag2 + " - I will become a mobile developer");
    flag2 -= 2;
}
console.log("");

console.log("No. 2 Looping menggunakan for");
var ganjilGenap, kelipatanTiga;
for(var num = 1; num <= 20; num++) {
    ganjilGenap = num%2;
    kelipatanTiga = num%3;
    if(ganjilGenap == 0) {
        console.log(num + " - Berkualitas");
    } else if(ganjilGenap != 0 && kelipatanTiga == 0) {
        console.log(num + " - I Love Coding")
    } else {
        console.log(num + " - Santai");
    }
}
console.log("");
console.log("No. 3 Membuat Persegi Panjang");
var tagar;
for(var flag1 = 1; flag1 <= 4; flag1++) {
    tagar = "#";
    for(var flag2 = 1; flag2 < 8; flag2++) {
        tagar = tagar.concat("#");
    }
    console.log(tagar);
}
console.log("");

console.log("No. 4 Membuat Tangga");
var flag1 = 1;
var flag2 = 1;
var tagar = "";
while(flag1 <= 7) {
    while(flag2 <= flag1) {
        tagar = tagar.concat("#");
        flag2++;
    }
    console.log(tagar);
    flag1++;
}
console.log("");

console.log("No. 5 Membuat Papan Catur");
var ganjilGenap1, ganjilGenap2;
for(var flag1 = 1; flag1 <= 8; flag1++) {
    var tagar = "";
    ganjilGenap1 = flag1%2;
    for(var flag2 = 1; flag2 <= 8; flag2++) {
        ganjilGenap2 = flag2%2;
        if(ganjilGenap1 != 0) {
            if(ganjilGenap2 != 0) {
                tagar = tagar.concat(" ");
            } else {
                tagar = tagar.concat("#");
            }
        } else {
            if(ganjilGenap2 != 0) {
                tagar = tagar.concat("#");
            } else {
                tagar = tagar.concat(" ");
            }
        }
    }
    console.log(tagar);
}