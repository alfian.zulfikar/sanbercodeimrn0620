// Soal No. 1 (Array to Object)
function arrayToObject(arr) {
    var now = new Date();
    var thisYear = now.getFullYear();
    var age;
    var bio = {};
    for (var i = 0; i < arr.length; i++) {
        if (arr[i][3] != null && arr[i][3]<thisYear) {
            age = thisYear-arr[i][3]
        } else {
            age = "Invalid birth year";
        }
        var obj = {
            firstName: arr[i][0],
            lastName: arr[i][1],
            gender: arr[i][2],
            age: age
        }
        console.log((i+1) + ". " + obj.firstName + " " + obj.lastName + ": ");
        console.log(obj);
    }
    
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
arrayToObject(people);
console.log("");
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

console.log("");

// Soal No. 2 (Shopping Time)
function shoppingTime(memberId, money) {
    var firstMoney = money;
    var listPurchased = [];
    var barang = [
        ["Sepatu Stacattu", 1500000],
        ["Baju Zoro", 500000],
        ["Baju H&N", 250000],
        ["Sweater Uniklooh", 175000],
        ["Casing Handphone", 50000]
    ];

    if (memberId != null && memberId != '' && money != null) {
        if (money >= barang[4][1]) {
            for (var i = 0; i < barang.length; i++) {
                if (money >= barang[i][1]) {
                    listPurchased = [barang[i][0]];
                    money = money-barang[i][1];
                }
            }
            
            var customer = {
                memberId: memberId,
                money: firstMoney,
                listPurchased: listPurchased,
                changeMoney: money
            }
        
            return customer;
        } else {
            return "Mohon maaf, uang tidak cukup";
        }
    } else {
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    }
    // return barang[4][1];
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log("")

// Soal No. 3 (Naik Angkot)
function naikAngkot(arrPenumpang) {
    var penumpang = [];
    var tujuan = false;
    var asal = false
    var ongkos = 0;
    var result = [];
    var obj = {};
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    for (var h = 0; h < arrPenumpang.length; h++) {
        for(var i = 0; asal == false; i++) {
            if (rute[i] == arrPenumpang[h][1]) {
                var mulai = i+1;
                // ongkos = 2000;
                for (var j = mulai; tujuan == false; j++) {
                    ongkos = ongkos + 2000;
                    if (rute[j] == arrPenumpang[h][2]) {
                        tujuan = true;
                    }
                }
                asal = true;
            }
        }
        obj = {
            penumpang: arrPenumpang[h][0],
            naikDari: arrPenumpang[h][1],
            tujuan: arrPenumpang[h][2],
            bayar: ongkos
        }
        ongkos = 0;
        asal = false;
        tujuan = false;
        result[h] = obj;
    }
    return result;
}
   
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
// console.log(naikAngkot([])); //[]